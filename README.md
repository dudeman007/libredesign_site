# Libre Design
**Site is part of finals project for group 2**


This site is not complete, and lacking in many features and final touches. Also The code is a mess, cheers!

* Mobile view is non-existent and totally crap **Do not view on mobile**
* Contacts form has no action yet, therefor you cannot send any mail. It just disappears into the ether.
* Some design inconsistencies with the logo and logo font. Meh.
* Navigation bar needs some tweaking to make it consistent with the other pages.
* About page is not completed!
* Need to split sass "employee" from globals.sass to its' own file, eg. employee.sass


Feel free to contribute before our dead line. **Don't forget to create a pull request for your contributed changes**

**Deadline = Tues. April 30th 2019**
